#!/usr/bin/python3
# test.py -- a simple Python3 test page
# Copyright (C) 2017 Arman Hajishafieha
#
#  This file is part of shttpd.
#  shttpd is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  shttpd is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with shttpd.  If not, see <http://www.gnu.org/licenses/>.
import os;

print("Content-type: text/html");

page = """\
<!DOCTYPE html>
<html>
    <head><title>Python3 CGI test page</title></head>
    <body>
        <h1>Hello World!</h1>
        <p>
            This is a test page to demonstrate python3 CGI script<br>
            Your IP address is: {remote_addr}<br>
            Your user agent: {ua}<br>
            Host: {host}<br>
            Query string: {query}
        </p>
        <p>Powered by {server}</p>
    </body>
</html>""";
page = page.format(remote_addr = os.environ["REMOTE_ADDR"], \
        ua = os.environ["HTTP_USERAGENT"], host = os.environ["HTTP_HOST"], \
        query = os.environ["QUERY_STRING"], \
        server = os.environ["SERVER_SOFTWARE"]);
print("Content-Length: " + str(len(page)));
print("");
print(page);
