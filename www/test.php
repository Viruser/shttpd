<?php
# test.php -- a simple PHP test page
# Copyright (C) 2017 Arman Hajishafieha
#
#  This file is part of shttpd.
#  shttpd is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  shttpd is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with shttpd.  If not, see <http://www.gnu.org/licenses/>.
?>

<!DOCTYPE html>
<html>
    <head><title>PHP CGI test page</title></head>
    <body>
        <h1>Hello World!</h1>
        <p>
            This is a test page to demonstrate PHP script running as CGI<br>
            Your IP address is: <?php echo $_SERVER['REMOTE_ADDR']; ?><br>
            Your user agent: <?php echo $_SERVER['HTTP_USERAGENT']; ?><br>
            Host: <?php echo $_SERVER['HTTP_HOST']; ?><br>
            Query string: <?php echo $_SERVER['QUERY_STRING']; ?>
        </p>
        <p>Powered by <?php echo $_SERVER['SERVER_SOFTWARE'] ?></p>
    </body>
</html>