#! /usr/bin/python3
# shttpd -- Stupid http daemon
# Copyright (C) 2017 Arman Hajishafieha
#
#  This file is part of shttpd.
#  shttpd is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  shttpd is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with shttpd.  If not, see <http://www.gnu.org/licenses/>.

import socket;
import os;
import subprocess;
import mimetypes;

program_name = "shttpd";
program_version = "0.2.0";
port = 8080;
listenip = "";
dirlisting = 1;
cgiexts = {"py": "", "py3": "", "php": "php-cgi", "cgi": "", "pl": "", "pl6": ""};
vhosts = [ {"host": "localhost", "docroot": "/var/www/html"}];
#Note: for PHP to work docroot must be set to a full path. otherwise you get a no input file error.
vhost_default = {"host": "default", "docroot": "./www"};

def main():
	print("This is " + program_name + " " + program_version + " http server\n");
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1);
		s.bind((listenip, port));
		s.listen(1);
		while 1:
			connection, addr = s.accept();
			res = shttpd_process_req(connection, addr);
			connection.send(res);
			connection.close();


def shttpd_process_req(socket, addr):
	req = shttpd_get_request(socket);
	if not isinstance(req, dict):
		return req;
	vhost = None;
	for vh in vhosts:
		if vh["host"] == req["host"].split(":")[0].lower():
			vhost = vh;
			break;
	if vhost == None:
		vhost = vhost_default;
	relpath = req["shttpd_url"].split("?")[0];
	if shttpd_url_check(relpath) == -1:
		return shttpd_gen_response(403, "Koor khoondi aii koor khoondi");
	ext = shttpd_get_fileext(relpath).lower();
	if ext in cgiexts:
		return shttpd_runcgi(req, addr, ext, vhost);
	try:
		fhandle = open(vhost["docroot"] + relpath, "rb");
		data = fhandle.read();
		fhandle.close();
		ctype = mimetypes.guess_type(relpath);
		if ctype[0] == None:
			ctype = ["application/octet-stream"];
		resfields = { "Content-Length": str(len(data)), "Content-Type": ctype[0] };
		return shttpd_gen_response(200, "OK", resfields, data);
	except FileNotFoundError:
		return shttpd_gen_response(404, "Not found");
	except PermissionError:
		return shttpd_gen_response(403, "Permission denied");
	except IsADirectoryError:
		if dirlisting:
			return shttpd_gen_dirlist(vhost["docroot"], relpath);
		else:
			return shttpd_gen_response(403, "Permission denied");
	except:
		return shttpd_gen_response(500, "Unknown error");

def shttpd_get_fileext(path):
	index = path.rfind(".");
	if index == -1:
		return "";
	return path[index + 1:];

def shttpd_get_request(sockethdl):
	req = dict.fromkeys(["shttpd_method", "shttpd_url", "shttpd_content", "user-agent", "host", "cookie", "referer", \
	                      "content-length", "content-type"]);
	reqdata = sockethdl.recv(65536).decode();
	headerfields = reqdata.splitlines(0);
	header = headerfields[0].split(" ");
	if len(header) != 3 or header[2] != "HTTP/1.1":
		return shttpd_gen_response(400, "Bad request");
	req["shttpd_method"] = header[0];
	req["shttpd_url"] = header[1];

	while not "" in headerfields:
		reqdata += sockethdl.recv(65536).decode();
		headerfields = reqdata.splitlines(keepends = 0);

	for field in headerfields[1:-1]:
		namevalue = field.split(sep=":", maxsplit=1);
		if len(namevalue) != 2:
			return shttpd_gen_response(400, "Bad request");
		namevalue[0] = namevalue[0].strip();
		namevalue[1] = namevalue[1].strip();
		if namevalue[0].lower() in req.keys():
			req[namevalue[0].lower()] = namevalue[1];

	if req["content-length"] != None and int(req["content-length"]) != 0:
		reqdata += sockethdl.recv(req["content-length"]).decode();
		for marker in ["\r\n\r\n", "\n\n", "\r\r"]:
			offset = reqdata.find(marker);
			if offset != -1:
				offset += len(marker);
				req["shttpd_content"] =  reqdata[offset: offset + int(req["content-length"])];
				break;
	if req["shttpd_url"][0] != "/":
		shttpd_gen_response(400, "Bad request");
	if req["shttpd_method"] != "GET" and req["shttpd_method"] != "POST":
		return shttpd_gen_response(405, "Method not allowed");
	return req;

def shttpd_gen_response(code, msg, fields = {}, content = None):
	print("Run with code " + str(code) + " msg: " + msg);
	response = "HTTP/1.1 " + str(code) + " " + msg + "\r\n";
	ctype_key = "";
	clen_key = "";
	server_key = "";
	for key in fields.keys():
		if key.lower() == "content-type":
			ctype_key = key;
		elif key.lower() == "content-length":
			clen_key = key;
		elif key.lower() == "server":
			server_key = key;
		if not key.lower() == "content-length" or key.lower() == "content-type":
			response += key + ": " + fields[key] + "\r\n";
	if content != None:
		if clen_key == "":
			clen = str(len(content));
		else:
			clen = fields[clen_key];
		if ctype_key == "":
			return shttpd_gen_response(500, "Internal server error");
		else:
			ctype = fields[ctype_key];
	elif code >= 400 and code <= 599:
		content = str(code) + " " + msg;
		content = content.encode();
		clen = str(len(content));
		ctype = "text/html";
	else:
		clen = "0";
		ctype = None;

	if server_key == "":
		response += "Server: " + program_name + " " + program_version + "\n";

	response += "Content-Length: " + clen + "\r\n";
	if ctype != None:
		response += "Content-Type: " + ctype + "\r\n";
	response += "\r\n";
	if content != None:
		return response.encode() + content;
	return response.encode();


def shttpd_url_check(url):
	tree = url.split("/");
	level = 0;
	if tree[0] != "":
		return -1;
	for d in tree[1:]:
		if d == "..":
			level -= 1;
		elif not d == "." or "":
			level += 1;
		if level < 0:
			return -1;
	return 0;

def shttpd_list_dir(docroot, url):
	dirlist = os.scandir(docroot + url);
	if url[len(url) - 1] == "/":
		url = url[:-1];
	index = url.rfind("/");
	up = url.rpartition("/");
	if up[2] != "":
		if up[0] == "":
			dirs = [{"name": "../", "path": "/"}];
		else:
			dirs = [{"name": "../", "path": up[0]}];
	else:
		dirs = [];
	files = [];
	for entry in dirlist:
		if entry.is_dir():
			dirs.append({"name": entry.name + "/", "path": url + "/" + entry.name});
		else:
			files.append({"name": entry.name, "path": url + "/" + entry.name});
	return dirs + files;

def shttpd_gen_dirlist(docroot, url):
	files = shttpd_list_dir(docroot, url);
	dirlist = "";
	for f in files:
		dirlist += "<a href=\"" + f["path"] + "\">" + f["name"] + "</a><br>\n";
	output = """<!DOCTYPE html>
<html>
	<head><title>Index of {url}</title></head>
	<body>
		<h1>Index of {url}</h1>
		<div>{dirlist}</div>
		<br><br>
		<p>Powered by {name} {version}</p>
	</body>
</html>"""
	output = output.format(name = program_name, version = program_version, url = url, dirlist = dirlist);
	return shttpd_gen_response(200, "OK", {"content-type": "text/html"}, output.encode());

def shttpd_runcgi(req, addr, ext, vhost):
	query = req["shttpd_url"].split("?", maxsplit = 1);
	os.environ["REMOTE_ADDR"] = addr[0];
	os.environ["REQUEST_METHOD"] = req["shttpd_method"];
	os.environ["SCRIPT_NAME"] = query[0];
	os.environ["SERVER_NAME"] = listenip;
	os.environ["SERVER_PORT"] = str(port);
	os.environ["SERVER_PROTOCOL"] = "HTTP/1.1";
	os.environ["SERVER_SOFTWARE"] = program_name + " " + program_version;
	os.environ["HTTP_HOST"] = req["host"];
	os.environ["REQUEST_URI"] = req["shttpd_url"];
	os.environ["REDIRECT_STATUS"] = "1";
	os.environ["SCRIPT_FILENAME"] = vhost["docroot"] + query[0];
	if req["cookie"] != None:
		os.environ["HTTP_COOKIE"] = req["cookie"];
	else:
		os.environ["HTTP_COOKIE"] = "";
	if req["referer"] != None:
		os.environ["HTTP_REFERER"] = req["referer"];
	else:
		os.environ["HTTP_REFERER"] = "";
	if req["user-agent"] != None:
		os.environ["HTTP_USERAGENT"] = req["user-agent"];
	else:
		os.environ["HTTP_USERAGENT"] = "";
	if req["content-type"] != None:
		os.environ["CONTENT_TYPE"] = req["content-type"];
	else:
		os.environ["CONTENT_TYPE"] = "";
	if len(query) > 1:
		os.environ["QUERY_STRING"] = query[1];
	else:
		os.environ["QUERY_STRING"] = "";

	if req["shttpd_method"] == "POST" and len(req["shttpd_content"]) != 0:
		data = req["shttpd_content"];
		os.environ["CONTENT_LENGTH"] = req["content-length"];
	else:
		data = None;
		os.environ["CONTENT_LENGTH"] = "0";
	try:
		if cgiexts[ext] != "":
			res = subprocess.run([cgiexts[ext], vhost["docroot"] + query[0]], stdout = subprocess.PIPE, input = data);
		else:
			res = subprocess.run(vhost["docroot"] + query[0], stdout = subprocess.PIPE, input = data);
		output = res.stdout.decode();
		status = [200, ""];
		ctype = "";
		outfields = {};
		headerfields = output.splitlines();
		for field in headerfields:
			if field == "":
				break;
			namevalue = field.split(sep=":", maxsplit=1);
			if len(namevalue) != 2:
				return shttpd_gen_response(500, "Internal server error");
			namevalue[0] = namevalue[0].strip();
			namevalue[1] = namevalue[1].strip();
			if namevalue[0].lower() == "status":
				status = field.split(sep=" ", maxsplit=1);
				if len(status) != 2:
					return shttpd_gen_response(500, "Internal server error");
				continue;
			if namevalue[0].lower() == "content-type":
				ctype = namevalue[1];
			outfields[namevalue[0]] = namevalue[1];
		index = output.find("\r\n\r\n");
		if index == -1:
			index = output.find("\n\n");
			if index == -1:
				index = output.find("\r\r");
				if index == -1:
					return shttpd_gen_response(500, "Internal server error");
			index += 2;
		else:
			index += 4;

		if ctype == "":
			return shttpd_gen_response(500, "Internal server error");

		return shttpd_gen_response(status[0], status[1], outfields, output[index:].encode());
	except:
		return shttpd_gen_response(502, "Bad Gateway");
if __name__== "__main__":
   main();
